FROM ubuntu:18.04
ENV PACKAGES openssl faketime
RUN apt-get update && apt-get -y install ${PACKAGES}
RUN mkdir data
WORKDIR "/data"
